package main


type Exit struct {
  text string
  description string
  scene_id int
}