package main


type Scene struct {
  name string
  text string
  exit_ids [5]int
}

func (scene Scene) num_exits() int {
  count := 0
  for i := 0; i < len(scene.exit_ids); i++ {
    if (scene.exit_ids[i] == 0) {
      break
    }
    count++
  }
  return count
}

func (scene Scene) get_exit(index int) Exit {
  exits := scene.num_exits()
  if (index > exits) {

  }
  exit_id := scene.exit_ids[index - 1]
  exit := exit_data[exit_id]
  return exit
}