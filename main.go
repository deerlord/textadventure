package main

import "fmt"
import "math/rand"
import "time"


func main(){
  rand.Seed(time.Now().UTC().UnixNano())
  scene := scene_data[1]
  for (scene.num_exits() != 0) {
    scene = loop(scene)
    fmt.Println("\033[2J")
  }
  fmt.Println(scene.text)
  display_gameover()
}

func loop(scene Scene) Scene {
  display_scene(scene)
  display_exits(scene)
  exit := handle_exits(scene)
  scene = scene_data[exit.scene_id]
  return scene
}

func handle_exits(scene Scene) Exit {
  input := 0
  fmt.Print("\nWhat will you do? ")
  for (input == 0){
    input = pick_choice(scene.num_exits())
  }
  // get the exit and resulting outcome
  exit := scene.get_exit(input)
  return exit
}

func pick_choice(amount int) int {
  var input int
  fmt.Scanln(&input)
  if (1 <= input && input <= amount) {
    return input
  }
  return 0
}

