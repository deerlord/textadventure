package main

var scene_data = map[int]Scene{
  1: {"Outside the Grand Gate", "You stand outside the castle gate.", [5]int{1, 7}},
  2: {"The Foyer", "You walk into a grand foyer.", [5]int{2, 3}},
  3: {"The Upstairs Hallway", "You stand at the top of the staircase.", [5]int{4, 5, 8}},
  4: {"The Kitchen", "You stand in the kitchen, the fire roaring.", [5]int{6}},
  5: {"Generic Death", "You are mortally wounded. Your body collapses into a heap.", [5]int{}},
  6: {"Woods Death", "You retreat into the woods. The darkness slowly engulfs you...", [5]int{}},
  7: {"Balcony Death", "Caught up in the smell, you fail to realize you have walked off the balcony...", [5]int{}},
  8: {"Gurgle Death", "You hear a *splat* as you progress down the hallway. Your boot appears to be stuck in a thick puddle of liquid. Then, you look up...", [5]int{}},
}

var exit_data = map[int]Exit{
  1: {"Enter the castle", "A lowered drawbridge and raised gate await you.", 2},
  2: {"Go up the stairs", "A central staircase spirals up and out of sight.", 3},
  3: {"Go to the kitchen", "A faint glow emanates from a nearby doorway.", 4},
  4: {"Head towards the smell", "A familiar smell wafts from down the hall.", 7},
  5: {"Head towards the sound", "In the distance, you hear a low gurgling noise.", 8},
  6: {"Walk through archway", "The foyer lies in clear view through the archway.", 2},
  7: {"Retreat to the woods", "The woods from where you just came lie behind you.", 6},
  8: {"Go down the stairs", "A staircase extends down, to the foyer.", 2},
}
