package main

import "fmt"

func display_scene(scene Scene){
  fmt.Println(scene.name)
  fmt.Println()
  fmt.Print(scene.text)
}

func display_exits(scene Scene){
  description_text := ""
  choice_text := ""
  num_exits := scene.num_exits()
  for i := 1; i <= num_exits; i++ {
    exit := scene.get_exit(i)
    description_text += fmt.Sprintf(" %s", exit.description)
    choice_text += fmt.Sprintf("%d) %s\n", i, exit.text)
  }
  fmt.Printf("%s\n\n%s", description_text, choice_text)
}

func display_gameover() {
  fmt.Println()
  fmt.Println("YOU DIED")
}